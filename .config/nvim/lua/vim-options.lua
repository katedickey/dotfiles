vim.cmd("set expandtab")
vim.cmd("set autoindent")
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=2")
vim.cmd("set shiftwidth=2")
vim.cmd("set number")
vim.g.mapleader = " "
vim.o.timeout = true
vim.o.timeoutlen = 300

if vim.fn.has("unnamedplus") == 1 then
    vim.cmd("set clipboard=unnamedplus")
else
    vim.cmd("set clipboard=unnamed")
end
