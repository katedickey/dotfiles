return {
  {
    'nvim-telescope/telescope.nvim', branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      { 'echasnovski/mini.icons', version = false },
      { 'folke/which-key.nvim', optional = true,
        opts = {
          spec = {
            {"<leader>f", group = "+Telescope" },
          },
        },
      }
    },
    keys = function()
      local telescope = require("telescope.builtin")
      return {
        { "<leader>fb", telescope.buffers, desc = "Buffers" },
        { "<leader>ff", telescope.find_files, desc = "Find file" },
        { "<leader>fg", telescope.live_grep, desc = "Grep" },
        { "<leader>fh", telescope.help_tags, desc = "Help" },
        { "<leader>fr", telescope.oldfiles, desc = "Recent files" },
      }
    end
  },
  {
    'nvim-telescope/telescope-ui-select.nvim',
    config = function()
      require("telescope").setup {
        extensions = {
          ["ui-select"] = {
            require("telescope.themes").get_dropdown {}
          }
        }
      }
      require("telescope").load_extension("ui-select")
    end
  }
}
