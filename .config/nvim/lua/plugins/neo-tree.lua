return {
  "nvim-neo-tree/neo-tree.nvim",
  branch = "v3.x",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",
    "MunifTanjim/nui.nvim",
  },
  opts = {
    source_selector = {
      winbar = true
    },
  },
  keys = {
    {'<leader>n', ':Neotree filesystem reveal left<CR>', desc = "Neotree" },
    {'<leader>b', ':Neotree buffers reveal left<CR>', desc = "Neotree buffers" },
    {'<leader>v', ':Neotree git_status reveal left<CR>', desc = "Neotree git status" },
  }
}
