return {
  {
    "williamboman/mason.nvim",
    config = true
  },
  {
    "williamboman/mason-lspconfig.nvim",
    opts = {
      ensure_installed = { "lua_ls" }
    }
  },
  {
    "neovim/nvim-lspconfig",
    config = function()
      require("mason-lspconfig").setup_handlers {
        function (server_name) -- default handler (optional)
            require("lspconfig")[server_name].setup {}
        end,
        -- Next, you can provide a dedicated handler for specific servers.
        -- For example, a handler override for the `rust_analyzer`:
        -- ["rust_analyzer"] = function ()
        --     require("rust-tools").setup {}
        -- end
    }

      vim.api.nvim_create_autocmd('LspAttach', {
        callback = function(args)
          local util = require('custom/util')
          local opts = { noremap = true, silent = true, buffer = args.buf }
          vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, util.merge(opts, { desc = "Declaration" }))
          vim.keymap.set('n', 'gd', vim.lsp.buf.definition, util.merge(opts, { desc = "Definition" }))
          vim.keymap.set('n', 'K', vim.lsp.buf.hover, util.merge(opts, { desc = "Hover" }))
          vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, util.merge(opts, { desc = "Implementation" }))
          vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, util.merge(opts, { desc = "Signature help" }))
          vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, util.merge(opts, { desc = "Add workspace folder" }))
          vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, util.merge(opts, { desc = "Remove workspace folder" }))
          vim.keymap.set('n', '<leader>wl', function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
          end, util.merge(opts, { desc = "List workspace folders" }))
          vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, util.merge(opts, { desc = "Type definition" }))
          vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, util.merge(opts, { desc = "Rename" }))
          vim.keymap.set('n', 'gr', vim.lsp.buf.references, util.merge(opts, { desc = "References" }))
          vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, util.merge(opts, { desc = "Open float" }))
          vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, util.merge(opts, { desc = "Previous diagnostic" }))
          vim.keymap.set('n', ']d', vim.diagnostic.goto_next, util.merge(opts, { desc = "Next diagnostic" }))
          vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, util.merge(opts, { desc = "Diagnostic list" }))
          vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, util.merge(opts, { desc = "Code action" }))
        end
      })
    end
  }
}
