. $HOME/.zsh/utils
dotfiles_update
if [[ $? == 3 ]]; then
  echo Restarting zsh
  exec zsh
fi

# Path to your oh-my-zsh installation.
ZSH=$HOME/.zsh/oh-my-zsh/

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME=""

if [ ! -f $HOME/.zsh/prompt_color ]; then
    PROMPT_COLOR=$(color_str $HOST)
    echo $PROMPT_COLOR > $HOME/.zsh/prompt_color
else
    PROMPT_COLOR=$(<$HOME/.zsh/prompt_color)
fi

export TYPEWRITTEN_PROMPT_LAYOUT="singleline_verbose"
export TYPEWRITTEN_SYMBOL=">"
export TYPEWRITTEN_COLOR_MAPPINGS="primary:#ff00ff;secondary:cyan"
export TYPEWRITTEN_COLORS="user:${PROMPT_COLOR};host:red"

fpath+="$HOME/.zsh/typewritten"
autoload -U promptinit; promptinit
prompt typewritten

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

if command -v dircolors &> /dev/null; then
    eval "$(dircolors $HOME/.zsh/dir_colors)"
fi

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  wd
)

# eval "$(zoxide init --cmd cd zsh)"

# source <(fzf --zsh)

# User configuration

path+=($HOME/.local/bin $HOME/.bin)

export EDITOR="nvim"
export VISUAL=$EDITOR

export GPG_TTY=$TTY

export FZF_COMPLETION_TRIGGER='~~'

# Disable flow control
stty stop undef
stty start undef

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir -p $ZSH_CACHE_DIR
fi

export ANDROID_HOME=~/Android/Sdk
export ANDROID_SDK_ROOT=$ANDROID_HOME

if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
  . '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi

for f in $HOME/.zsh/zshrc.d/*(N); do
    source $f
done

# terminfo

export USER_TERMINFO=$HOME/.local/share/terminfo
if [[ -z `find $USER_TERMINFO -not -iname '*.terminfo'` ]]; then
  echo Building terminfo...
  find $USER_TERMINFO -type f -iname '*.terminfo' -exec tic -xo $USER_TERMINFO {} \;
fi
export TERMINFO_DIRS=${TERMINFO_DIRS:-}${TERMINFO_DIRS:+:}$USER_TERMINFO

source $ZSH/oh-my-zsh.sh

